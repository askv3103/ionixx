var express = require("express");
var session = require("express-session");
var path = require("path");
const cors = require("cors");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var config = require("config");
var app = express();
const port = process.env.PORT || 8000;
var Router = require("./routes/index");

//MongoDB Connection

const mongoose = require("mongoose");
// const MongoClient = require("mongodb").MongoClient;
const uri = config.get("mongoURI");
// const assert = require("assert");
mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => console.log("The MongoDB Server is Running Successfully!!"))
  .catch((err) => console.log(err));


// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.set("views", "views");
app.options("*", cors());
app.use(cors({}));


app.use(express.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json({ limit: "50mb", extended: true }));
app.use(express.static(path.join("public")));
app.use(
  session({
    secret: "asdf;lkj",
    resave: false,
    saveUninitialized: false,
    cookie: {},
  })
);

Router(app);


app.listen(port, function () {
  console.log("Server Running on Port : " + port);
});

module.exports = { app };
