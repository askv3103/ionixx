const express = require("express");
const router = express.Router();
const { postSomething } = require("../controllers/authController");

router.post("/", postSomething);

module.exports = router;
