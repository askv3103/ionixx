const express = require("express");
const admin = require("./admin");
const auth = require("./auth");

module.exports = (app) => {
  app.use("/admin", admin);
  app.use("/auth", auth);

  app.use("/", (req, res) => {
    res.send("Welcome to backend!!");
  });
};
