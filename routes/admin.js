const express = require("express");
const router = express.Router();
const {
  getSomething,
  postCreateAdmin,
  getAdminLogin,
  postAdminLogin,
  getLogout,
  postAddBus,
} = require("../controllers/adminController");

router.get("/", getSomething);
router.post("/create-admin", postCreateAdmin);
router.get("/admin-login", getAdminLogin);
router.post("/admin-login", postAdminLogin);
router.get("/logout", getLogout);
router.get("/addBus", postAddBus);



module.exports = router;
