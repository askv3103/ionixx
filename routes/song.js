const express = require("express");
var passport = require("passport");
const router = express.Router();
const passportConfig = require("../services/passport");
const {
  postMusic,
  getMusic,
  updateMusic,
  getTrack,
  postTrack,
  // postMusicUpload,
} = require("../controllers/songController");

router.get("/:trackID", getMusic);
router.post("/", postMusic);
router.patch("/:trackID", updateMusic);
router.get("/track/:trackID", getTrack);
router.post("/track", postTrack);
// router.post("/uploads", postMusicUpload);

module.exports = router;
