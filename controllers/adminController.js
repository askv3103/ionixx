const mongodb = require("mongodb");
var config = require("config");
const session = require("express-session");
const crypto = require("crypto");
const fs = require("fs");
const assert = require("assert");
var { client, bus, Agent} = require("../models/product");
const path = require("path");



function hash(input, salt) {
  //creating hash
  var hashed = crypto.pbkdf2Sync(input, salt, 10000, 512, "sha512");
  return ["pbkdf2", "10000", salt, hashed.toString("hex")].join("$");
}

exports.getSomething = (req, res) => {
  res.send("Welcome !!");
};

exports.postCreateAdmin = (req, res) => {
    console.log(session);
    let newAdmin = new client(req.body);
    let password = newAdmin.password;
    console.log(password);
    let salt = crypto.randomBytes(128).toString("hex");
    let hashedPassword = hash(password, salt);
    newAdmin.password = hashedPassword;
    console.log(newAdmin.password);
    newAdmin
      .save()
      .then(() => {
        return res.send("Admin added Successfully");
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).send("Adding new admin failed");
      });
};

exports.getAdminLogin = (req, res) => {
  res.send("Hi Admin");
};

exports.postAdminLogin = (req, res) => {
  var admin_details = req.body;
  var user = client.findOne(
    { username: admin_details.username },
    (err, user) => {
      if (err) throw err;
      if (user) {
        var realpass = user.password;
        salt = realpass.split("$")[2];
        admin_hash = hash(admin_details.password, salt);
        if (admin_hash === user.password) {
          req.session.admin = true;
          req.session._id = user._id;
          req.session.cookie.maxAge = 3 * 24 * 3600 * 1000;
          // console.log(Date.now() + 3 * 24 * 3600 * 1000);
          // req.session.admin.expire = new Date(
          //   Date.now() + 3 * 24 * 3600 * 1000
          // );
          res.status(200).send("Admin Successfully loggedin");
        } else {
          res.status(403).send("Incorrect Password");
        }
      } else {
        (err) => console.log(err);
        res.status(400).send("Invalid Login Credentials");
      }
    }
  );
};


//Logout 
exports.getLogout = (req, res) => {
  req.session.username = "";
  req.session.cookie.expire = true;
  req.session.cookie.maxAge = 0;
  res.redirect("/");
};

exports.postAddBus = (req, res) => {
  if(req.session.admin){
    var newBus = new bus(req.body);
    console.log(newBus);
    if (busName != null && from != null && to != null && totalSeat != null && price != null){
      newBus
      .save();
      res.status(400).send("Bus added successfully");
    }
     else res.send("fill in the complete details")
  }
  
  
};

exports.postAddAgent = (req, res) => {
  if(req.session.admin){
    var newAgent = new Agent(req.body);
    console.log(newAgent);
    if (username != null && password != null && phone != null){
      newAgent.save();
      res.status(400).send("Agent added successfully");
    }
    else res.send("fill in the complete details")
  }
  
};

