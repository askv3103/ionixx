const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let agent = new Schema({
  username: { type: String, unique: true },
  password: String,
  phone: Number
});

let client = new Schema({
  username: {type : String , unique : true},
  password: String
});

let bus = new Schema({
  busName : {type: String, unique: true},
  from: String,
  to: String, 
  totalSeat: Number,
  price: Number
})




const Agent_schema = mongoose.model("Agent ", agent);
const admin_schema = mongoose.model("client ", client);
const bus_schema = mongoose.model("bus ", bus);
// user_schema.plugin(findOrCreate);
module.exports = {
  client: admin_schema,
  Agent: Agent_schema,
  bus: bus_schema,
};
