const passport = require("passport");
const dotenv = require("dotenv").config();
const mongoose = require("mongoose");
// const bcrypt = require("bcrypt");

const LocalStrategy = require("passport-local");
var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;

const jwtStrategy = require("passport-jwt").Strategy,
  ExtractJwt = require("passport-jwt").ExtractJwt;

var findOrCreate = require("mongoose-findorcreate");
var Schema = mongoose.Schema;
var UserSchema = new Schema({ googleId: Number });
UserSchema.plugin(findOrCreate);
var User = mongoose.model("User", UserSchema);

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromExtractors([
    ExtractJwt.fromAuthHeaderWithScheme("JWT"),
    ExtractJwt.fromUrlQueryParameter("token"),
  ]),
  secretOrKey: "process.env.SECRET",
  passReqToCallback: true,
};

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
});

const localOption = {
  usernameField: "username",
  passwordField: "password",
  passReqToCallback: true,
};

// const localLogin = new LocalStrategy(localOption, function (
//   req,
//   username,
//   password,
//   done
// ) {
//   console.log("inside localstrategy");

//   db.query(query, [username], (err, result) => {
//     if (err) return done(err);
//     if (result.rows.length == 0) return done(null, false);

//     bcrypt.compare(password, result.rows[0].password, function (err, isMatch) {
//       if (err) return done(err, false);
//       if (!isMatch) return done(null, false);
//       let u = result.rows[0];
//       return done(null, { ...u });
//     });
//   });
// });

const jwtLogin = new jwtStrategy(jwtOptions, function (req, payload, done) {
  console.log("inside jwt", payload);

  db.query(query, [payload.id], function (err, user) {
    if (err) {
      console.log(err);
      return done(err, false);
    }
    if (user.rows.length == 0) {
      return done(null, false);
    } else {
      let u = { ...user.rows[0], type: req.header.type };
      return done(null, { ...u });
    }
  });
});

const localLogin = new LocalStrategy(function (username, password, done) {
  User.findOne({ username: username }, function (err, user) {
    if (err) {
      return done(err);
    }
    if (!user) {
      return done(null, false, { message: "Incorrect username." });
    }
    if (!user.validPassword(password)) {
      return done(null, false, { message: "Incorrect password." });
    }
    return done(null, user);
  });
});

const googleLogin = new GoogleStrategy(
  {
    clientID: process.env["GOOGLE_CLIENT_ID"],
    clientSecret: process.env["GOOGLE_CLIENT_SECRET"],
    callbackURL: "http://localhost:8000/admin/return",
  },
  function (accessToken, refreshToken, profile, done) {
    User.findOrCreate({ googleId: profile.id }, function (err, user) {
      return done(err, user);
    });
  }
);

passport.use("jwt", jwtLogin);
passport.use("local", localLogin);
passport.use("google", googleLogin);

